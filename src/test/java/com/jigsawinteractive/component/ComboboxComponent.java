/*
* ComboboxComponent.java
*
* Created at 23.01.2021 by Magdalena Nankova <magdalena.nankova@gmail.com>
*
*/
package com.jigsawinteractive.component;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a wrapper for select dropdown.
 * It provides helper method to change the selected value in the dropdown.
 * It is reused in Page Object Model
 * 
 * @author magdalena.nankova
 *
 */
public class ComboboxComponent {
	private static final Logger LOG = LoggerFactory.getLogger(ComboboxComponent.class);

	private final WebDriver driver;
	private final WebElement element;

	/**
	 * Constructor for combobox wrapper component
	 * 
	 * @param driver WebDriver - the selenium web driver
	 * @param elementId - the html element id
	 */
	public ComboboxComponent(WebDriver driver, String elementId) {
		super();
		this.driver = driver;
		String inputId = elementId + "_input";
		this.element = this.driver.findElement(By.xpath("//input[@type='text' and @name='" + inputId + "']"));
	}

	/**
	 * Select a new option value in the dropdown
	 * 
	 * @param value String - the value to select in the dropdown list
	 */
	public void selectOption(String value) {
		LOG.info("selectOption value[{}]", value);
		element.clear();
		element.sendKeys(value);
		
		String listboxId = element.getAttribute("aria-owns");
		// select option
		String xpath = "//*[@id='" + listboxId  +"']/li[text()='" + value + "']";
		LOG.info("click xpath[{}]...", xpath);
		WebElement optionElement = new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
		optionElement.click();
	}

}
