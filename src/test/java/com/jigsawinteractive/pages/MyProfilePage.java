/*
* MyProfilePage.java
*
* Created at 23.01.2021 by Magdalena Nankova <magdalena.nankova@gmail.com>
*
*/
package com.jigsawinteractive.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jigsawinteractive.component.ComboboxComponent;

/**
 * Page Object Model representing the My Profile page
 * 
 * @author magdalena.nankova
 *
 */
public class MyProfilePage {
	private static final Logger LOG = LoggerFactory.getLogger(MyProfilePage.class);

	private final WebDriver driver;

	/**
	 * Constructor for My Profile page
	 * 
	 * @param driver WebDriver - the selenium web driver
	 */
	public MyProfilePage(WebDriver driver) {
		super();
		this.driver = driver;
	}

	/**
	 * Return true if the page is loaded correctly
	 * 
	 * @return boolean
	 */
	public boolean isPageOpened() {
		String title = this.driver.getTitle();
		LOG.info("isPageOpened title[{}]", title);
		return title.equals("Virtual Learning - Register");
	}

	/**
	 * Read the value of textbox email
	 * 
	 * @return String - the value
	 */
	public String getEmail() {
		WebElement emailElement = this.driver.findElement(By.id("EmailAddress"));
		String result = emailElement.getAttribute("value");
		LOG.info("getEmail[{}]", result);
		return result;
	}

	/**
	 * Click button Continue.
	 */
	public void clickButtonContinue() {
		LOG.info("clickButtonContinue");
		this.driver.findElement(By.id("btnContinue")).click();
	}

	/**
	 * Click button Save.
	 * The My Sessions page is loaded in the browser
	 * 
	 * @return MySessionsPage - the POM for My Sessions page
	 */
	public MySessionsPage clickButtonSave() {
		LOG.info("clickButtonSave");
		this.driver.findElement(By.id("saveButton")).click();
		
		return new MySessionsPage(this.driver);
	}

	/**
	 * Enter value for textbox First name
	 * 
	 * @param value String - the value
	 */
	public void enterFirstName(String value) {
		LOG.info("enterFirstName[{}]", value);
		WebElement element = this.driver.findElement(By.id("FirstName"));
		element.sendKeys(value);
	}

	/**
	 * Enter value for textbox Last name
	 * 
	 * @param value String - the value
	 */
	public void enterLastName(String value) {
		LOG.info("enterLastName[{}]", value);
		WebElement element = this.driver.findElement(By.id("LastName"));
		element.sendKeys(value);
	}

	/**
	 * Select value in dropdown Country
	 * 
	 * @param value String - the value
	 */
	public void selectCountry(String value) {
		LOG.info("selectCountry[{}]", value);
		ComboboxComponent comboboxComponent = new ComboboxComponent(driver, "CountryId");
		comboboxComponent.selectOption(value);
	}

	/**
	 * Select value in dropdown State
	 * 
	 * @param value String - the value
	 */
	public void selectState(String value) {
		LOG.info("selectState[{}]", value);
		ComboboxComponent comboboxComponent = new ComboboxComponent(driver, "RegionId");
		comboboxComponent.selectOption(value);
	}

	/**
	 * Select value in dropdown City
	 * 
	 * @param value String - the value
	 */
	public void selectCity(String value) {
		LOG.info("selectCity[{}]", value);
		ComboboxComponent comboboxComponent = new ComboboxComponent(driver, "CityId");
		comboboxComponent.selectOption(value);
	}

	/**
	 * Select value in dropdown Time zone
	 * 
	 * @param value String - the value
	 */
	public void selectTimeZone(String value) {
		LOG.info("selectTimeZone[{}]", value);
		ComboboxComponent comboboxComponent = new ComboboxComponent(driver, "TimeZoneInfoId");
		comboboxComponent.selectOption(value);
	}

	/**
	 * Enter value for textbox Password
	 * 
	 * @param value String - the value
	 */
	public void enterPassword(String value) {
		LOG.info("enterPassword[{}]", value);
		WebElement element = this.driver.findElement(By.id("Password"));
		element.sendKeys(value);
	}

	/**
	 * Enter value for textbox Confirm password
	 * 
	 * @param value String - the value
	 */
	public void enterConfirmPassword(String value) {
		LOG.info("enterConfirmPassword[{}]", value);
		WebElement element = this.driver.findElement(By.id("ConfirmPassword"));
		element.sendKeys(value);
	}

}
