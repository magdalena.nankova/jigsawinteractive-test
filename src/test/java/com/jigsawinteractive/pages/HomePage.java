/*
* HomePage.java
*
* Created at 23.01.2021 by Magdalena Nankova <magdalena.nankova@gmail.com>
*
*/
package com.jigsawinteractive.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Page Object Model representing the home page
 * 
 * @author magdalena.nankova
 *
 */
public class HomePage {
	private static final Logger LOG = LoggerFactory.getLogger(HomePage.class);

	private final WebDriver driver;

	// Page URL
	// on dev environment the get country list and geoip do not work - so use production
//	private static String PAGE_URL = "https://app.dev.jigsawinteractive.com/";
	private static String PAGE_URL = "https://app.jigsawinteractive.com/";

	/**
	 * Constructor for home page
	 * 
	 * @param driver WebDriver - the selenium web driver
	 */
	public HomePage(WebDriver driver) {
		super();
		this.driver = driver;
	}

	/**
	 * Open the URL for home page
	 */
	public void openPage() {
		LOG.info("openPage url[{}]", PAGE_URL);
		this.driver.get(PAGE_URL);
	}

	/**
	 * Return true if the page is loaded correctly
	 * 
	 * @return boolean
	 */
	public boolean isPageOpened() {
		String title = this.driver.getTitle();
		LOG.info("isPageOpened title[{}]", title);
		return title.equals("Virtual Learning - Login");
	}

	/**
	 * Click button First Time User.
	 * The login page is loaded in the browser
	 * 
	 * @return LoginPage - the POM for login page
	 */
	public LoginPage clickButtonFirstTimeUser() {
		LOG.info("clickButtonFirstTimeUser");
		this.driver.findElement(By.id("btnFirstTimeUser")).click();
		
		return new LoginPage(this.driver);
	}

}
