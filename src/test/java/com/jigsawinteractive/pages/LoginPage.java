/*
* LoginPage.java
*
* Created at 23.01.2021 by Magdalena Nankova <magdalena.nankova@gmail.com>
*
*/
package com.jigsawinteractive.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Page Object Model representing the login page
 * 
 * @author magdalena.nankova
 *
 */
public class LoginPage {
	private static final Logger LOG = LoggerFactory.getLogger(LoginPage.class);

	private final WebDriver driver;

	/**
	 * Constructor for login page
	 * 
	 * @param driver WebDriver - the selenium web driver
	 */
	public LoginPage(WebDriver driver) {
		super();
		this.driver = driver;
	}

	/**
	 * Return true if the page is loaded correctly
	 * 
	 * @return boolean
	 */
	public boolean isPageOpened() {
		String title = this.driver.getTitle();
		LOG.info("isPageOpened title[{}]", title);
		WebElement welcomeElement = this.driver.findElement(By.className("first-time-user-visible"));
		String welcomeText = welcomeElement.getText();
		boolean displayed = welcomeElement.isDisplayed();
		LOG.info("welcomeText [{}] displayed[{}]", welcomeText, displayed);
		return title.equals("Virtual Learning - Login") && displayed && welcomeText.contentEquals("Welcome! Please enter your email address to continue.");
	}

	/**
	 * Enter the value in email textbox
	 * 
	 * @param value String - the value to enter
	 */
	public void enterEmail(String email) {
		LOG.info("enterEmail[{}]", email);
		WebElement emailElement = this.driver.findElement(By.id("txtEmail"));
		emailElement.sendKeys(email);
	}

	/**
	 * Click button Continue.
	 * The My Profile page is loaded in the browser
	 * 
	 * @return MyProfilePage - the POM for My Profile page
	 */
	public MyProfilePage clickButtonContinue() {
		LOG.info("clickButtonContinue");
		this.driver.findElement(By.id("btnContinue")).click();
		
		return new MyProfilePage(this.driver);
	}

}
