/*
* MySessionsPage.java
*
* Created at 23.01.2021 by Magdalena Nankova <magdalena.nankova@gmail.com>
*
*/
package com.jigsawinteractive.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Page Object Model representing the My Profile page
 * 
 * @author magdalena.nankova
 *
 */
public class MySessionsPage {
	private static final Logger LOG = LoggerFactory.getLogger(MySessionsPage.class);

	private final WebDriver driver;

	/**
	 * Constructor for My Sessions page
	 * 
	 * @param driver WebDriver - the selenium web driver
	 */
	public MySessionsPage(WebDriver driver) {
		super();
		this.driver = driver;
	}

	/**
	 * Return true if the page is loaded correctly
	 * 
	 * @return boolean
	 */
	public boolean isPageOpened() {
		LOG.info("isPageOpened");
		new WebDriverWait(driver, 5000L).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='listSessionsUpcoming']")));
		String documentTitle = this.driver.getTitle();
		LOG.info("isPageOpened documentTitle[{}]", documentTitle);
		return documentTitle.equals("Virtual Learning - My Sessions");
	}
	
	/**
	 * Return true if table with sessions in tab Upcomming is empty
	 * 
	 * @return boolean
	 */
	public boolean isUpcommingSessionsEmpty() {
		LOG.info("isUpcommingSessionsEmpty");
		List<WebElement> elements = this.driver.findElements(By.xpath("//div[@id='listSessionsUpcoming']/*"));
		return elements.isEmpty();
	}

}
