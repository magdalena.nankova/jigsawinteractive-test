/*
* FirstTimeUserPageFactoryTest.java
*
* Created at 23.01.2021 by Magdalena Nankova <magdalena.nankova@gmail.com>
*
*/
package com.jigsawinteractive.tests;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.jigsawinteractive.factory.ChromeWebDriverFactory;
import com.jigsawinteractive.factory.WebDriverFactory;
import com.jigsawinteractive.pages.HomePage;
import com.jigsawinteractive.pages.LoginPage;
import com.jigsawinteractive.pages.MyProfilePage;
import com.jigsawinteractive.pages.MySessionsPage;
import com.jigsawinteractive.utils.SleepUtils;

/**
 * JUnit4 Test Cases using Page Object Model
 * 
 * @author magdalena.nankova
 *
 */
public class FirstTimeUserTest {

	private WebDriver driver;

	/**
	 * Execute before each test method to initialize the web driver 
	 */
	@Before
	public void beforeEachTestMethod() {
		WebDriverFactory factory = new ChromeWebDriverFactory();
		this.driver = factory.create();
		this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	/**
	 * Execute after each test method to close the web driver 
	 */
	@After
	public void afterEachTestMethod() {
		this.driver.close();
	}

	/**
	 * A test for "First-time user" scenario
	 */
	@Test
	public void createFirstTimeUser() {
		String email = "magdalena.nankova+" + UUID.randomUUID().toString() + "@gmail.com";
		
		HomePage homePage = new HomePage(driver);
		
		// Go to https://app.dev.jigsawinteractive.com/
		homePage.openPage();
		Assert.assertTrue("expected home page to be opened", homePage.isPageOpened());
		
		// Click the "First time user" button
		LoginPage loginPage = homePage.clickButtonFirstTimeUser();
		// Verify that Login page is open
		Assert.assertTrue("expected login page to be opened", loginPage.isPageOpened());

		// Enter a valid email address
		loginPage.enterEmail(email);
		// Click the "Continue" button
		MyProfilePage myProfilePage = loginPage.clickButtonContinue();
		// Verify that My Profile page is open
		Assert.assertTrue("expected my profile page to be opened", myProfilePage.isPageOpened());

		// Assert that your email address is pre-populated in the "Email address" field		
		Assert.assertEquals(email, myProfilePage.getEmail());
		
		// Fill the required fields
		String password = "abcABC123";
		myProfilePage.enterFirstName("Magdalena");
		myProfilePage.enterLastName("Nankova");
		myProfilePage.enterPassword(password);
		myProfilePage.enterConfirmPassword(password);
		// wait for geoip resolve and get country list - I couldn't find a better way to determine when is done
		SleepUtils.sleep(5000L);
		myProfilePage.selectCountry("Bulgaria");
		myProfilePage.selectState("Sofiya");
		myProfilePage.selectCity("Sofiya");
		myProfilePage.selectTimeZone("(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius");
		
		// Click the "Save" button
		MySessionsPage mySessionsPage = myProfilePage.clickButtonSave();
		
		// Assert that the Home page (My Sessions) is open and the session list is empty
		Assert.assertTrue("expected my sessions page to be opened", mySessionsPage.isPageOpened());
		Assert.assertTrue("expected Upcomming list to be empty", mySessionsPage.isUpcommingSessionsEmpty());
	}
}
