/*
* WebDriverFactory.java
*
* Created at 23.01.2021 by Magdalena Nankova <magdalena.nankova@gmail.com>
*
*/
package com.jigsawinteractive.factory;

import org.openqa.selenium.WebDriver;

/**
 * Factory interface to create instance of Selenium web driver
 * 
 * @author magdalena.nankova
 *
 */
public interface WebDriverFactory {

	/**
	 * Create new instance and return it
	 * 
	 * @return WebDriver - the create Selenium web driver
	 */
	public WebDriver create();
}
