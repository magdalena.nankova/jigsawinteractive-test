package com.jigsawinteractive.factory;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jigsawinteractive.utils.OSFileUtils;

/**
 * Factory implementation to create instance of Selenium web driver for
 * Google Chrome browser
 * 
 * @author magdalena.nankova
 *
 */
public class ChromeWebDriverFactory implements WebDriverFactory {
	private static final Logger LOG = LoggerFactory.getLogger(ChromeWebDriverFactory.class);

	private static final String DRIVER_PATH_PROPERTY = "webdriver.chrome.driver";
	private static final String REMOTE_SELENIUM_ENV = "REMOTE_SELENIUM";
	private static final String SELENIUM_HOST_ENV = "SELENIUM_HOST";

	/**
	 * Create new instance and return it
	 * 
	 * @return WebDriver - the create Selenium web driver
	 */
	@Override
	public WebDriver create() {
		String remoteSelenium = System.getenv(REMOTE_SELENIUM_ENV);
		if (remoteSelenium != null && remoteSelenium.equals("true")) {
			// remote
			LOG.info("remote driver selected");
			String selenoumHost = System.getenv(SELENIUM_HOST_ENV);
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.addArguments("--disable-dev-shm-usage");
			URL remoteAddress;
			try {
				remoteAddress = new URL("http://" + selenoumHost + ":4444/wd/hub");
			} catch (MalformedURLException e) {
				LOG.error("cannot crete remoteAddress selenoumHost[" + selenoumHost + "]", e);
				throw new RuntimeException(e);
			}
			LOG.info("remoteAddress[{}]", remoteAddress);
			return new RemoteWebDriver(remoteAddress, chromeOptions);
		} else {
			// local
			LOG.info("local driver selected");
			String propertyValue = System.getProperty(DRIVER_PATH_PROPERTY);
			if (propertyValue == null) {
				String path = "chromedriver_" + OSFileUtils.getOSFileSuffix();
				LOG.info("set environment varible [{}] with path[{}]", DRIVER_PATH_PROPERTY, path);
				System.setProperty(DRIVER_PATH_PROPERTY, path);
			} else {
				LOG.info("environment varible [{}]=[{}] already set - skip set the path", DRIVER_PATH_PROPERTY, propertyValue);
			}
			return new ChromeDriver();
		}
	}

}
