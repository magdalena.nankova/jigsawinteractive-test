/*
* LoginPage.java
*
* Created at 23.01.2021 by Magdalena Nankova <magdalena.nankova@gmail.com>
*
*/
package com.jigsawinteractive.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Page Object Model with Selenium Page Factory representing the login page
 * 
 * @author magdalena.nankova
 *
 */
public class LoginPage {
	private static final Logger LOG = LoggerFactory.getLogger(LoginPage.class);

	private final WebDriver driver;

	@FindBy(id = "txtEmail")
	private WebElement email;

	@FindBy(id = "btnContinue")
	private WebElement buttonContinue;

	@FindBy(className = "first-time-user-visible")
	private WebElement welcomeElement;

	/**
	 * Constructor for login page
	 * 
	 * @param driver WebDriver - the selenium web driver
	 */
	public LoginPage(WebDriver driver) {
		super();
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Return true if the page is loaded correctly
	 * 
	 * @return boolean
	 */
	public boolean isPageOpened() {
		String title = this.driver.getTitle();
		LOG.info("isPageOpened title[{}]", title);
		String welcomeText = welcomeElement.getText();
		boolean displayed = welcomeElement.isDisplayed();
		LOG.info("welcomeText [{}] displayed[{}]", welcomeText, displayed);
		return title.equals("Virtual Learning - Login") && displayed && welcomeText.contentEquals("Welcome! Please enter your email address to continue.");
	}

	/**
	 * Enter the value in email textbox
	 * 
	 * @param value String - the value to enter
	 */
	public void enterEmail(String value) {
		LOG.info("enterEmail[{}]", email);
		email.sendKeys(value);
	}

	/**
	 * Click button Continue.
	 * The My Profile page is loaded in the browser
	 * 
	 * @return MyProfilePage - the POM for My Profile page
	 */
	public MyProfilePage clickButtonContinue() {
		LOG.info("clickButtonContinue");
		buttonContinue.click();
		
		return new MyProfilePage(this.driver);
	}

}
