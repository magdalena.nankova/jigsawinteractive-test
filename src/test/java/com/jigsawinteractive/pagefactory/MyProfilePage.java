/*
* MyProfilePage.java
*
* Created at 23.01.2021 by Magdalena Nankova <magdalena.nankova@gmail.com>
*
*/
package com.jigsawinteractive.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jigsawinteractive.component.ComboboxComponent;

/**
 * Page Object Model with Selenium Page Factory representing the My Profile page
 * 
 * @author magdalena.nankova
 *
 */
public class MyProfilePage {
	private static final Logger LOG = LoggerFactory.getLogger(MyProfilePage.class);

	private final WebDriver driver;

	@FindBy(id = "EmailAddress")
	private WebElement email;

	@FindBy(id = "btnContinue")
	private WebElement buttonContinue;

	@FindBy(id = "saveButton")
	private WebElement buttonSave;

	@FindBy(id = "FirstName")
	private WebElement firstName;

	@FindBy(id = "LastName")
	private WebElement lastName;

	@FindBy(id = "Password")
	private WebElement password;

	@FindBy(id = "ConfirmPassword")
	private WebElement confirmPassword;

	private final ComboboxComponent countryComboboxComponent;
	
	private final ComboboxComponent regioncomboboxComponent;

	private final ComboboxComponent cityComboboxComponent;

	private final ComboboxComponent timzeZoneInfoComboboxComponent;

	/**
	 * Constructor for My Profile page
	 * 
	 * @param driver WebDriver - the selenium web driver
	 */
	public MyProfilePage(WebDriver driver) {
		super();
		this.driver = driver;
		PageFactory.initElements(driver, this);
		this.countryComboboxComponent = new ComboboxComponent(driver, "CountryId");
		this.regioncomboboxComponent = new ComboboxComponent(driver, "RegionId");
		this.cityComboboxComponent = new ComboboxComponent(driver, "CityId");
		this.timzeZoneInfoComboboxComponent = new ComboboxComponent(driver, "TimeZoneInfoId");

	}

	/**
	 * Return true if the page is loaded correctly
	 * 
	 * @return boolean
	 */
	public boolean isPageOpened() {
		String title = this.driver.getTitle();
		LOG.info("isPageOpened title[{}]", title);
		return title.equals("Virtual Learning - Register");
	}

	/**
	 * Read the value of textbox email
	 * 
	 * @return String - the value
	 */
	public String getEmail() {
		String result = email.getAttribute("value");
		LOG.info("getEmail[{}]", result);
		return result;
	}

	/**
	 * Click button Continue.
	 */
	public void clickButtonContinue() {
		LOG.info("clickButtonContinue");
		buttonContinue.click();
	}
	
	/**
	 * Click button Save.
	 * The My Sessions page is loaded in the browser
	 * 
	 * @return MySessionsPage - the POM for My Sessions page
	 */
	public MySessionsPage clickButtonSave() {
		LOG.info("clickButtonSave");
		buttonSave.click();
		
		return new MySessionsPage(this.driver);
	}

	/**
	 * Enter value for textbox First name
	 * 
	 * @param value String - the value
	 */
	public void enterFirstName(String value) {
		LOG.info("enterFirstName[{}]", value);
		firstName.sendKeys(value);
	}

	/**
	 * Enter value for textbox Last name
	 * 
	 * @param value String - the value
	 */
	public void enterLastName(String value) {
		LOG.info("enterLastName[{}]", value);
		lastName.sendKeys(value);
	}

	/**
	 * Select value in dropdown Country
	 * 
	 * @param value String - the value
	 */
	public void selectCountry(String value) {
		LOG.info("selectCountry[{}]", value);
		countryComboboxComponent.selectOption(value);
	}

	/**
	 * Select value in dropdown State
	 * 
	 * @param value String - the value
	 */
	public void selectState(String value) {
		LOG.info("selectState[{}]", value);
		regioncomboboxComponent.selectOption(value);
	}

	/**
	 * Select value in dropdown City
	 * 
	 * @param value String - the value
	 */
	public void selectCity(String value) {
		LOG.info("selectCity[{}]", value);
		cityComboboxComponent.selectOption(value);
	}

	/**
	 * Select value in dropdown Time zone
	 * 
	 * @param value String - the value
	 */
	public void selectTimeZone(String value) {
		LOG.info("selectTimeZone[{}]", value);
		timzeZoneInfoComboboxComponent.selectOption(value);
	}

	/**
	 * Enters value for textbox Password
	 * 
	 * @param value String - the value
	 */
	public void enterPassword(String value) {
		LOG.info("enterPassword[{}]", value);
		password.sendKeys(value);
	}

	/**
	 * Enters value for textbox Confirm password
	 * 
	 * @param value String - the value
	 */
	public void enterConfirmPassword(String value) {
		LOG.info("enterConfirmPassword[{}]", value);
		confirmPassword.sendKeys(value);
	}

}
