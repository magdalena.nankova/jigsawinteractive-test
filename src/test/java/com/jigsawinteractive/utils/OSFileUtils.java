/*
* OSUtils.java
*
* Created at 23.01.2021 by Magdalena Nankova <magdalena.nankova@gmail.com>
*
*/
package com.jigsawinteractive.utils;

/**
 * Utility class to get file suffix based on current operation system (OS)
 * 
 * @author magdalena.nankova
 *
 */
public class OSFileUtils {
	public static String OS = System.getProperty("os.name").toLowerCase();

	/**
	 * Return true if OS is Windows
	 * 
	 * @return boolean
	 */
	public static boolean isWindows() {
		return OS.indexOf("win") >= 0;
	}

	/**
	 * Return true if OS is Mac
	 * 
	 * @return boolean
	 */
	public static boolean isMac() {
		return OS.indexOf("mac") >= 0;
	}

	/**
	 * Return true if OS is Linux
	 * 
	 * @return boolean
	 */
	public static boolean isLinux() {
		return OS.indexOf("linux") >= 0;
	}

	/**
	 * Return file suffix, which should be added to the file to get the full name based on the current OS
	 * 
	 * @return String
	 */
	public static String getOSFileSuffix() {
		if (isWindows()) {
			return "win.exe";
		}
		if (isMac()) {
			return "mac";
		}
		if (isLinux()) {
			return "linux";
		}
		throw new IllegalStateException("unsupported OS[" + OS + "]");
	}

}
