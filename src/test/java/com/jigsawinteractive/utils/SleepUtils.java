/*
* SleepUtils.java
*
* Created at 23.01.2021 by Magdalena Nankova <magdalena.nankova@gmail.com>
*
*/
package com.jigsawinteractive.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class to sleep for period of time in milliseconds
 * 
 * @author magdalena.nankova
 *
 */
public class SleepUtils {
	private static final Logger LOG = LoggerFactory.getLogger(SleepUtils.class);

	/**
	 * sleep provided millis as argument and re-throws the checked exception as runtime unchecked exception
	 * 
	 * @param millis long - the time to sleep in milliseconds
	 */
	public static void sleep(long millis) {
		LOG.info("sleep millis[{}]", millis);
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}
}
