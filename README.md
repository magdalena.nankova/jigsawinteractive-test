# Java Selenium UI Test Project for https://app.jigsawinteractive.com

## Description

This project provides a solution to interview process QA assignment | Prime Holding. 

Please follow the steps:
```
1. Setup a new Java Selenium project
2. Create a test for "First-time user" scenario
2.1 Go to https://app.dev.jigsawinteractive.com/
2.2 Click the "First time user" button
2.3 Verify that Login page is open
2.4 Enter a valid email address
2.5 Click the "Continue" button
2.6 Verify that My Profile page is open
- Assert that your email address is pre-populated in the "Email address" field
2.7 Fill the required fields
2.8 Click the "Save" button
2.9 Assert that the Home page (My Sessions) is open and the session list is empty
```

## Prerequisites

- JDK 8 or higher
- Maven 3 or higher

## Usage

The following command will launch the tests execution:

    mvn clean test

To run with remote driver, you can run:

    REMOTE_SELENIUM=true SELENIUM_HOST=<host> mvn clean test

In this case the driver will use remote address http://`<host>`:4444/wd/hub

Replace `mvn` with `mvn.exe` in the above examples if you're on Windows.

The build is setup to work with Chrome.

Note: Currently the build supports Windows, Mac OS and Linux.

## File Structure

    .
    ├── src/test/java           # Maven source test Java files
    ├── target                  # Temporary maven build directory
    ├── chromedriver_linux      # Linux OS WebDriver for Chrome
    ├── chromedriver_mac        # Mac OS WebDriver for Chrome
    ├── chromedriver_win.exe    # Windows OS WebDriver for Chrome
    └── README.md               # This file

Note: for production projects binary files like chromedriver_* should not be commit with project source code, but downloaded during build process. Added in project source for simplicity.

## Implementation

Technologies and tools used in project:
- maven - for dependecies management and build tool
- GitLab - for source code git repository
- GitLab CI - for build server (see file `.gitlab-ci.yml`)
- JUnit 4 - for writing and executing Test Cases
- Java language - for writing the code.
- SLF4J - for logging
- maven-surefire-plugin - for running JUnit 4 test with maven integration

This test class uses Page Object Model files in package com.jigsawinteractive.pages:
```
com.jigsawinteractive.tests.FirstTimeUserTest
```

This test class uses Page Object Model with Selenium Page Factory files in package com.jigsawinteractive.pagefactory:
```
com.jigsawinteractive.tests.FirstTimeUserPageFactoryTest
```

## Java Packages

    com.jigsawinteractive   
    ├── component               # Components, which can be use in diferent Page Objects
    │   └── ComboboxComponent   # Test component wrapper for Select Dropdown
    ├── factory                 # Contains WebDriverFactory and Chrome implementation ChromeWebDriverFactory
    ├── pagefactory             # Page Object Model with Selenium Page Factory
    ├── pages                   # Page Object Model
    ├── tests                   # The JUnit 4 Test Cases
    └── utils                   # Utilities classes

